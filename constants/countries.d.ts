export declare const countries: {
    name: string;
    dialCode: string;
    dialCodeNumber: string;
    code: string;
    flag: string;
}[];
