"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.booleans = exports.falses = exports.trues = void 0;
exports.trues = ["true", "1"];
exports.falses = ["false", "0"];
exports.booleans = [...exports.trues, ...exports.falses];
