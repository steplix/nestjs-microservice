"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.db = exports.Database = void 0;
/**
 * Database class administrator
 */
class Database {
}
exports.Database = Database;
// Singleton database instance
exports.db = new Database();
