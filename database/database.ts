/**
 * Database class administrator
 */
export class Database {}

// Singleton database instance
export const db = new Database();
