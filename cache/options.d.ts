/**
 * Indicate if Cache feature is enabled
 */
export declare const isCacheEnabled: boolean;
