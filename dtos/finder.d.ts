export declare class FinderDto {
    filters?: string;
    order?: string;
    group?: string;
    fields?: string;
    include?: string;
    remotes?: string;
    pageSize?: string;
    page?: string;
}
