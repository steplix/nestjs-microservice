import { HealthEntity } from "./entities";
export declare class HealthService {
    check(): Promise<HealthEntity>;
}
