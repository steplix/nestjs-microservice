export declare class HealthEntity {
    alive: boolean;
    name: string;
    version: string;
    environment: string;
    status?: string;
    info?: any;
    details?: any;
    error?: any;
}
